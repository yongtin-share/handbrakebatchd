#! /usr/bin/env ruby
require 'mysql'
require 'parseconfig'

filename = "handbrakebatchd.conf"
hbbRoot = File.dirname(File.dirname(File.expand_path(__FILE__)))
configFile = "#{hbbRoot}/etc/#{filename}"

data = ParseConfig.new(configFile)
sqlServer = data.get_value("mysqlHost")
sqlUser = data.get_value("mysqlUser")
sqlPassword = data.get_value("mysqlPass")
sqlDB = data.get_value("mysqlDB")
mysqldb = Mysql::new(sqlServer, sqlUser, sqlPassword, sqlDB)

# Show locking
lockJobs = mysqldb.query("SELECT tgt_absPath from tgt_yadvideos where tgt_locked = 1")
lockJobs.each do |lockJob|
	tgt_absPath = lockJob[0]
	puts "Currently locking: #{tgt_absPath}"
end

# Show running
runningJobs = mysqldb.query("SELECT tgt_absPath, conversion_host, conversion_timestart from convert_yadvideos where conversion_status = 0")
runningJobs.each do |runningJob|
	tgt_absPath, host, timestart = runningJob
	puts "Currently running on #{host} started at #{timestart}: #{tgt_absPath}"
end

