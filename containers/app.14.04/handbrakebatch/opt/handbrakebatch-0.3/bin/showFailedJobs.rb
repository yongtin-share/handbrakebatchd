#! /usr/bin/env ruby
require 'mysql'
require 'parseconfig'

filename = "handbrakebatchd.conf"
hbbRoot = File.dirname(File.dirname(File.expand_path(__FILE__)))
configFile = "#{hbbRoot}/etc/#{filename}"

data = ParseConfig.new(configFile)
sqlServer = data.get_value("mysqlHost")
sqlUser = data.get_value("mysqlUser")
sqlPassword = data.get_value("mysqlPass")
sqlDB = data.get_value("mysqlDB")
mysqldb = Mysql::new(sqlServer, sqlUser, sqlPassword, sqlDB)

# Show failed
failedJobs = mysqldb.query("SELECT tgt_filename, conversion_host, conversion_timestart from convert_yadvideos where conversion_status = 2")
failedJobs.each do |failedJob|
	tgt_filename, host, timestart = failedJob
	#puts "Currently failed on #{host} started at #{timestart}: #{tgt_absPath}"
	printf("%60s ===> %35s : %15s\n",tgt_filename, host,timestart)
end

