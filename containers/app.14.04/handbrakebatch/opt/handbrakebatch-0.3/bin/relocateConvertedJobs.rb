#! /usr/bin/env ruby
require 'mysql'

sqlServer = "mysqldb.lnx.yasid.sytes.net"
sqlUser = "handbrakebatch"
sqlPassword = "1234rfvcde"
sqlDB = "handbrakebatch"
mysqldb = Mysql::new(sqlServer, sqlUser, sqlPassword, sqlDB)

mysqldb.query("SET autocommit=0")
mysqldb.query("START TRANSACTION;")

# Get lists of old converted
# select tgt_absPath,tgt_converted from tgt_yadvideos where tgt_absPath like '/smbfs/dolphin%/yasidtv/yasidLife-C%';
oldpath="/videoexport/yasidLife/yasidPictures/YASID/YASID/2006/2006 Disneyland"
newpath="/videoexport/yasidLife/yasidPictures/YASID/2006/2006 Disneyland"
cameraJobs = mysqldb.query("SELECT tgt_absPath from tgt_yadvideos where tgt_absPath like '#{oldpath}%'")
cameraJobs.each do |job|
    tgt_absPath = job[0]
    newtgt_absPath = tgt_absPath.gsub(/#{oldpath}/, "#{newpath}")
    mysqldb.query("UPDATE tgt_yadvideos SET tgt_absPath = '#{newtgt_absPath}' WHERE tgt_absPath = '#{tgt_absPath}'")
    puts ("UPDATE tgt_yadvideos SET tgt_absPath = '#{newtgt_absPath}' WHERE tgt_absPath = '#{tgt_absPath}'")
end

#oldpath="/smbfs/dolphin.yasid.sytes.net/yasidtv/yasidLife-VideoRecorder"
#newpath="/videoexport/yasidLife/yasidVideo"
#videocamJobs = mysqldb.query("SELECT tgt_absPath from tgt_yadvideos where tgt_absPath like '/smbfs/dolphin.yasid.sytes.net/yasidtv/yasidLife-VideoRecorder%'")
#videocamJobs.each do |job|
#    tgt_absPath = job[0]
#    newtgt_absPath = tgt_absPath.gsub(/#{oldpath}/, "#{newpath}")
#    mysqldb.query("UPDATE tgt_yadvideos SET tgt_absPath = '#{newtgt_absPath}' WHERE tgt_absPath = '#{tgt_absPath}'")
#    puts ("UPDATE tgt_yadvideos SET tgt_absPath = '#{newtgt_absPath}' WHERE tgt_absPath = '#{tgt_absPath}'")
#end



mysqldb.query("COMMIT")

mysqldb.close
