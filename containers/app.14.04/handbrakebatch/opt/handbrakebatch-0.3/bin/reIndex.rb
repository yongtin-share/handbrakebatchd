#! /usr/bin/env ruby

class String
	def escapeChar 
		self.gsub(/'/, "''")
	end
end

        def tailingHash(file)
                #
                # Only scanning 210MB max for any files. 
                # This is to avoid running out of memory on large video files. 
                #
                require 'digest/md5'
                f = File.new(file, "r")
                byteSize = File.size(file).to_f
                gb = 1024*1024*1024
                mb = 1024*1024
                if byteSize > 4 * gb
                        tailbyteSize = 210 * mb
                else
                        tailbyteSize = (byteSize * 0.05).to_i
                end
                f.seek(-tailbyteSize, IO::SEEK_END)
                return Digest::MD5.hexdigest(f.read)
        end
#
# Reindex a directory to assume supported files under the directory are stable
#

if ARGV.length != 1
    puts "Usage: reIndex.rb <Directory>"
    exit 1
end

indexdir = ARGV[0]
require 'mysql'
require 'parseconfig'
require 'find'
require 'socket'

filename = "handbrakebatchd.conf"
hbbRoot = File.dirname(File.dirname(File.expand_path(__FILE__)))
configFile = "#{hbbRoot}/etc/#{filename}"

data = ParseConfig.new(configFile)
sqlServer = data.get_value("mysqlHost")
sqlUser = data.get_value("mysqlUser")
sqlPassword = data.get_value("mysqlPass")
sqlDB = data.get_value("mysqlDB")
supportedType = data.get_value("supportedType")
searchpattern = supportedType.gsub(",", "|")
sourceDir = data.get_value("sourceDir")
targetDir = data.get_value("targetDir")

mysqldb = Mysql::new(sqlServer, sqlUser, sqlPassword, sqlDB)

mysqldb.query("SET autocommit=0")
mysqldb.query("START TRANSACTION;")

Find.find(indexdir).each do |fileItem|
    if (not File.directory?fileItem) and fileItem =~ /(#{searchpattern})$/i
        # update target table (if you can) of course I can
        newfileName = File.basename(fileItem.gsub(File.extname(fileItem), ".mp4"))
        commonRelativePath = fileItem.gsub("#{sourceDir}/", "")
        commonRelativeDir = File.dirname(commonRelativePath)
        newVideoPath = File.expand_path("#{targetDir}/#{commonRelativeDir}/#{newfileName}")

	if not File.exists? newVideoPath
		puts "#{fileItem} needs to be converted. skipping during reIndexing. "
	elsif File.mtime(fileItem) == File.mtime(newVideoPath)
        # update source table 
            mysqldb.query("INSERT INTO src_yadvideos 
                (
                    src_filename,
                    src_mtime,
                    src_size,
                    src_absPath,
                    src_md5sum,
                    src_pass1,
                    src_pass2,
                    src_pass3,
                    src_stable
                )
                VALUES
                (
                    '#{File.basename(fileItem).escapeChar}',
                    '#{File.mtime(fileItem).strftime("%Y-%m-%d %H:%M:%S")}',
                    '#{File.size(fileItem)}',
                    '#{fileItem.escapeChar}',
                    '#{tailingHash(fileItem)}',
                    1, 1, 1, 1
                )
	")
        

	mysqldb.query("
	    INSERT INTO tgt_yadvideos
            (
                tgt_filename,
                tgt_absPath,
                src_absPath,
                tgt_locked,
                tgt_converted
            )
            VALUES 
            (
                '#{File.basename(newVideoPath).escapeChar}',
                '#{newVideoPath.escapeChar}',
                '#{fileItem.escapeChar}',
                0,
                1
            )
	")

	# updating conversion table also. assuming successful conversion given matching mtimes
	mysqldb.query("
	INSERT INTO convert_yadvideos
	(
                src_filename,
                src_absPath,
                tgt_filename,
                tgt_absPath,
                conversion_host,
                conversion_timestart,
                conversion_timeend,
                conversion_status
	) VALUES
	(
                '#{File.basename(fileItem).escapeChar}',
                '#{fileItem.escapeChar}',
                '#{File.basename(newVideoPath).escapeChar}',
                '#{newVideoPath.escapeChar}',
                '#{Socket.gethostbyname(Socket.gethostname).first}',
                NOW(),
                NOW(),
                1
	)
        ")
	elsif File.mtime(fileItem) != File.mtime(newVideoPath)
		puts "#{fileItem} mtime does not match with #{newVideoPath}. skipping Reindexing"
	end
    end
end

mysqldb.query("COMMIT")
mysqldb.close
