class HBBcli
    def initialize(hbbconfigHash)
        @config = hbbconfigHash
        
        require "#{@config[:hbbappRoot]}/lib/logmgmt"
        @hbblog = HBBLogging.new
        @hbblog.setLevel(@config[:loglevel])
        @hbblog.setLogFile(@config[:hbbappRoot]) if @config[:mode] == 'daemon'
        @hbblog.info "HandbrakeCLI management: Starting HandbrakeCLI management ... "
        
    end

    def mcbitrate_detection(srcpath,fps)
        #
        # Only used for rmvb videos
        #
        require 'open3'
        stdin,stdout,stderr = Open3.popen3("avconv -i '#{srcpath}'")
        stdin.puts("exit")
        while avconv = stderr.gets
            # Search for this in the output -> Stream #0.1: Video:
            bitrate = tbr = tbn = tbc = nil
            if avconv =~ /Stream #.*: Video:/
                bitrate = $1 if avconv =~ /, (\d+) kb\/s,/
                tbr = $1 if avconv =~ /, (\d+) tbr,/
                tbn = $1 if avconv =~ /tbr, ([\d\w]+) tbn,/
                tbc = $1 if avconv =~ /tbn, ([\d]+) tbc/
            elsif avconv =~ /Duration:/
                bitrate = $1 if avconv =~ /, bitrate: (\d+) kb\/s,/
                # Assuming all values are retrieved
                begin
                    bitperframe = bitrate.to_f / tbr.to_f
                    newbitrate = (bitperframe * fps).round(0)
                    return newbitrate
                rescue => e
                    @hbblog.warn "HandbrakeCLI management: Failed to detect bitrate. #{e}"
                    @hbblog.warn "HandbrakeCLI management: setting to 2048 bitrate"
                    return 2048.to_f
                end
            end
        end
    end
    
    def convert(srcpath, tgtpath)
        begin
            @hbblog.info "HandbrakeCLI management: Converting video #{srcpath} to #{tgtpath}"
            commandstatus = nil
            if not isRealMedia?(srcpath)
                @hbblog.debug "HandbrakeCLI management: Using HandbrakeCLI to perform conversion. "
                @hbblog.debug "HandbrakeCLI management: #{@config[:HBcommand]} #{@config[:HBoption]} -i #{srcpath} -o #{tgtpath} --preset #{@config[:presetOption]} > /dev/null"
                out = %x{
            
                    #{@config[:HBcommand]} #{@config[:HBoption]} -i "#{srcpath}" -o "#{tgtpath}" --preset "#{@config[:presetOption]}" > /dev/null
            
                }
                commandstatus = $?.exitstatus
            else
                @hbblog.debug "HandbrakeCLI management: Using avconv to perform conversion. Also performing mobile version"
                mobiletgtpath = tgtpath.gsub(/.mp4$/i, ".mobile.mp4")
                
                avconv_full_video_option = "-c:v libx264 -profile:v baseline -level 30"
                avconv_full_audio_option = "-c:a aac -strict experimental"
                
                avconv_mobile_video_option = "-c:v libx264 -crf 28 -crf_max 35 -maxrate 1M -bufsize 3M"
                avconv_mobile_audio_option = "-c:a aac -strict experimental"

                avconv_mobile_cmd = "avconv -y -i '#{srcpath}' #{avconv_mobile_video_option} #{avconv_mobile_audio_option} '#{mobiletgtpath}'"
                avconv_full_cmd = "avconv -y -i '#{srcpath}' #{avconv_full_video_option} #{avconv_full_audio_option} '#{tgtpath}'"

                @hbblog.debug "HandbrakeCLI management: RMVB mobule mobile command: #{avconv_mobile_cmd}"
                @hbblog.debug "HandbrakeCLI management: RMVB mobule full command: #{avconv_full_cmd}"
                @hbblog.info "HandbrakeCLI management: Executing ..."
                out = %x{

                    #{avconv_mobile_cmd}  &&
                    #{avconv_full_cmd}
			
                }
                commandstatus = $?.exitstatus

            end
    
            if commandstatus != 0
                @hbblog.debug "HandbrakeCLI management: convert command status non-zero: #{commandstatus}"
                raise "HandBrakeCLI exited with #{$?} status"
                raise "HandBrakeCLI: #{out}"
                return FALSE
            else
                @hbblog.debug "HandbrakeCLI management: convert command status returning zero: #{commandstatus}"
                return TRUE
            end
        rescue => e
            @hbblog.warn "HandbrakeCLI management: Converting video #{srcpath} to #{tgtpath} failed"
            @hbblog.warn "HandbrakeCLI management: #{e}"
            return FALSE
        end
    end
    
    protected
    
    def isRealMedia?(path)
        # This is not being used anymore.
        if path =~ /(.rm$|.rmvb$)/i
            return true
        else
            return false
        end
    end
end

