class HBBdbmgmt
	def initialize(hbbconfigHash)
		@config = hbbconfigHash
		
		require "#{@config[:hbbappRoot]}/lib/logmgmt"
		require "#{@config[:hbbappRoot]}/lib/configmgmt"
		@hbblog = HBBLogging.new
		@hbblog.setLevel(@config[:loglevel])
		@hbblog.setLogFile(@config[:hbbappRoot]) if @config[:mode] == 'daemon'

		@hbblog.info "Database management: Starting Database management system ..."
		
		@dbType = 'mysql'
		@dbLogin = {}
		case @dbType
		when 'mysql'
			@hbblog.info "Database management: Using mysql database - #{@config[:mysqlDB]}@#{@config[:mysqlHost]} " 
			@dbLogin = {
				:type => "#{@dbType}", 
				:dbname => "#{@config[:mysqlDB]}", 
				:dbhost => "#{@config[:mysqlHost]}",
				:dbuser => "#{@config[:mysqlUser]}",
				:dbpass => "#{@config[:mysqlPass]}"
			}
			require "#{@config[:hbbappRoot]}/lib/dbmgmthandle/hbbmysql"
		when 'sqlite3'
			@hbblog.info "Database management: using sqlite database "
			@dbLogin = {
				:type => "sqlite3",
				:dbname => "#{@config[:mysqlDB]}",
				:dbfile => "#{@config[:sqlFile]}"
			}
			require "#{@config[:hbbappRoot]}/lib/dbmgmthandle/hbbsqlite3"
		else
			@hbblog.error "Database management: unsupported database "
			raise "Unknown database type"
		end
		ensureDB
		
	end
end