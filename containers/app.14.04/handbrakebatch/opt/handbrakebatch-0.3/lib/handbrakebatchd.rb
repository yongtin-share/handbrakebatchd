class HandBrakeBatchd
	def initialize(dir)
		@appRoot = dir
		require "#{@appRoot}/lib/configmgmt"
		@config = HBBConfig.new(dir)
		@configHash = Hash.new
	end
	
	def parseCMDinput(cmd_input)
		require "#{@appRoot}/lib/configmgmt"
		@config.update_by_input(cmd_input)
	end
	
	def start
		require "#{@appRoot}/lib/daemonmgmt"
		configHash = @config.getConfig
		
		run = HBBDaemonMgmt.new(configHash)
		if configHash[:mode] == "interactive"
			run.interactive
		elsif configHash[:mode] == "daemon"
			run.daemon
		elsif configHash[:mode] == "scan"
			run.scan
		elsif configHash[:mode] == "resetDB"
			run.resetDB
		end
	end
end