#! /bin/bash

if [ $# -ne 1 ]; then
    exit 1
fi

app_tag_name="handbrakebatch_$1"
REPO="quay.io/yongtin/app"

# creating source code tarball
cd ./handbrakebatch/
tar -zcf ../handbrakebatch-source.tar.gz ./*
cd ../

# remove untagged images
#docker rm -f `docker ps -aq`
#docker rmi `docker images -f 'dangling=true' -q`

# build out docker image
timestamp=`date +%s`
docker build --no-cache=false -t=${REPO}:${app_tag_name}${timestamp} .

# publish docker image
docker push ${REPO}:${app_tag_name}${timestamp}
