#! /bin/bash

app_tag_name=`basename $PWD`

# build out docker image
docker build -t=quay.io/yongtin/coretest:${app_tag_name} .

# publish docker image
docker push quay.io/yongtin/coretest
