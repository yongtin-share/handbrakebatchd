class HBBIndexing
	def initialize(hbbconfigHash)
		@config = hbbconfigHash
		
		require "#{@config[:hbbappRoot]}/lib/logmgmt"
		@hbblog = HBBLogging.new
		@hbblog.setLevel(@config[:loglevel])
		@hbblog.setLogFile(@config[:hbbappRoot]) if @config[:mode] == 'daemon'
		@hbblog.info "Indexing management: Starting Indexing management system ... "
	end
	
	def isSupportedFile?(file)
		extFilename = File.extname(file)
		searchpattern = @config[:supportedType].gsub(",", "|")
		if extFilename =~ /(#{searchpattern})$/i
			@hbblog.debug "Indexing management: Found supported file - #{file}"
			return TRUE
		else
			return FALSE
		end
	end
	
	def findSourceFiles
		@hbblog.debug "Indexing management: Using file extension search option: #{@config[:supportedType]}"
		listOfVideos = Array.new
		require 'find'
		listOfFiles = Find.find(@config[:sourceDir])
		@hbblog.info "Indexing management: Searching for files under directory - #{@config[:sourceDir]}"
		listOfFiles.each do |fileItem|
			if (not File.directory?fileItem) && isSupportedFile?(fileItem)
				listOfVideos.push(fileItem)
			end
		end
		return listOfVideos
	end
end
