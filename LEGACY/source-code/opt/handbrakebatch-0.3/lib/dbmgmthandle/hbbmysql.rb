class String
	def escapeChar
		self.gsub(/'/, "''")
	end
end

class HBBdbmgmt
	require 'mysql'
	
	def resetDB
		@hbblog.debug "Database management: Dropping table in Database"
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		
		hbbTables = %w{src_yadvideos tgt_yadvideos convert_yadvideos}
		hbbTables.each do |table|
			@hbblog.info "Database management: Dropping table #{table}"
			mysqldb.query("DROP TABLE #{table}")
		end
	end
	
	def lockConvert(srcpath, tgtpath)
		@hbblog.debug "Database management: Locking #{tgtpath} video for conversion with source #{srcpath}. "
		require 'socket'
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		mysqldb.query("SET autocommit=0")
		mysqldb.query("START TRANSACTION;")
		mysqldb.query("INSERT INTO tgt_yadvideos 
			(
				tgt_filename,
				tgt_absPath,
				src_absPath,
				tgt_locked,
				tgt_converted
			)
			VALUES 
			(
				'#{File.basename(tgtpath).escapeChar}',
				'#{tgtpath.escapeChar}',
				'#{srcpath.escapeChar}',
				1,
				0
			)
		")
		mysqldb.query("INSERT INTO convert_yadvideos 
			(
				src_filename,
				src_absPath,
				tgt_filename,
				tgt_absPath,
				conversion_host,
				conversion_timestart,
				conversion_status
			) 
			VALUES 
			(
				'#{File.basename(srcpath).escapeChar}',
				'#{srcpath.escapeChar}',
				'#{File.basename(tgtpath).escapeChar}',
				'#{tgtpath.escapeChar}',
				'#{Socket.gethostbyname(Socket.gethostname).first}',
				NOW(),
				0
			)
		")
		mysqldb.query("COMMIT")		
		mysqldb.close
	end
	
	def unlockConvert(srcpath, tgtpath)
		@hbblog.debug "Database management: Unlocking #{tgtpath} video for conversion with source #{srcpath}. "
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		mysqldb.query("SET autocommit=0")
		mysqldb.query("START TRANSACTION;")
		mysqldb.query("UPDATE tgt_yadvideos SET 
			tgt_locked = 0, tgt_converted = 1 
			WHERE tgt_absPath = '#{tgtpath.escapeChar}'
		")
		mysqldb.query("UPDATE convert_yadvideos SET 
			conversion_status = 1,
			conversion_timeend = NOW()
			WHERE tgt_absPath = '#{tgtpath.escapeChar}' AND src_absPath = '#{srcpath.escapeChar}' AND conversion_status = 0
		")
		mysqldb.query("COMMIT")	
		mysqldb.close
	end
	
	def removeConvert(srcpath, tgtpath)
		@hbblog.debug "Database management: Removing #{tgtpath} video from conversion records with #{srcpath}. "
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		mysqldb.query("SET autocommit=0")
		mysqldb.query("START TRANSACTION;")
		mysqldb.query("DELETE FROM tgt_yadvideos WHERE tgt_absPath = '#{tgtpath.escapeChar}'")
		mysqldb.query("UPDATE convert_yadvideos SET
			conversion_status = 2,
			conversion_timeend = NOW()
			WHERE tgt_absPath = '#{tgtpath.escapeChar}' AND src_absPath = '#{srcpath.escapeChar}'
		")
		mysqldb.query("COMMIT")	
		mysqldb.close
	end
	
	def isStable?(video)
		@hbblog.debug "Database management: Checking if #{video.escapeChar} is stable for conversion"
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		yadrecords = mysqldb.query("SELECT src_stable FROM src_yadvideos WHERE src_absPath = '#{video.escapeChar}'")
		mysqldb.close
		
		if yadrecords.num_rows == 0
			return FALSE
		elsif yadrecords.num_rows == 1
			stable = yadrecords.fetch_row.first.to_i
			return TRUE if stable == 1
			return FALSE if stable == 0
		end
	end

	def isLocked?(video)
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		yadrecords = mysqldb.query("SELECT tgt_locked FROM tgt_yadvideos WHERE tgt_absPath = '#{video.escapeChar}'")
		mysqldb.close
		@hbblog.debug "Database management: Checking if #{video} has been locked for conversion"
		if yadrecords.num_rows == 0
			return FALSE
		elsif yadrecords.num_rows == 1
			tgt_locked = yadrecords.fetch_row.first.to_i
			if tgt_locked == 0
				return FALSE 
			else
				@hbblog.debug "Database management: #{video} has been locked for conversion"
				return TRUE 
			end
		end
	end
	
	def hasConverted?(srcPath, tgtPath)
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		yadrecords = mysqldb.query("SELECT tgt_converted FROM tgt_yadvideos WHERE tgt_absPath = '#{tgtPath.escapeChar}'")
		mysqldb.close
		@hbblog.debug "Database management: Checking if #{tgtPath} has been converted"
		if yadrecords.num_rows == 0
			@hbblog.debug "Database management: #{tgtPath} has not been converted"
			return FALSE
		elsif yadrecords.num_rows == 1
			@hbblog.debug "Database management: Getting #{tgtPath} conversion status. "
			tgt_converted = yadrecords.fetch_row.first.to_i
			if tgt_converted == 1
				return TRUE 
			else
				return FALSE 
			end
		elsif yadrecords.num_rows > 1
			@hbblog.warn "Database management: Found multiple conversion records for #{tgtPath}"
			@hbblog.debug "Database management: Getting #{tgtPath} conversion status. "
			tgt_converted = yadrecords.fetch_row.first.to_i
			if tgt_converted == 1
				return TRUE 
			else
				return FALSE 
			end
		end
	end
	
	def updateStatus(fileStatus)
		# fileStatus = { :path => video, :mtime => File.mtime(video), :fileSize => File.size(video) }
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		yadrecords = mysqldb.query("SELECT src_mtime, src_size, src_md5sum, src_absPath FROM src_yadvideos WHERE src_absPath = '#{fileStatus[:path].escapeChar}'")
		@hbblog.debug "Database management: Search mysql database for #{fileStatus[:path]}"
		if yadrecords.num_rows == 0
			@hbblog.debug "Database management: New record found - #{fileStatus[:path]}"
			# This is a new record
			newFilename = File.basename(fileStatus[:path])
			newPath = fileStatus[:path]
			newmTime = File.mtime(newPath).strftime("%Y-%m-%d %H:%M:%S")
			newSize = File.size(newPath)
			newHash = tailingHash(newPath)
			mysqldb.query("INSERT INTO src_yadvideos 
				(
					src_filename,
					src_mtime,
					src_size,
					src_absPath,
					src_md5sum,
					src_pass1,
					src_pass2,
					src_pass3,
					src_stable
				)
				VALUES
				(
					'#{newFilename.escapeChar}',
					'#{newmTime}',
					'#{newSize}',
					'#{newPath.escapeChar}',
					'#{newHash}',
					0, 0, 0, 0
				)
			")
			@hbblog.debug "Database management: New record inserted - #{fileStatus[:path]}"
		elsif yadrecords.num_rows == 1
			@hbblog.debug "Database management: Existing record found for #{fileStatus[:path]}"
			if remainStable(yadrecords.fetch_row)
				increaseStableStatus(fileStatus[:path])
			else
				backtoPass1(fileStatus[:path])
			end
		end

		mysqldb.close		
	end
	
	def getConvertedVideo(srcvideo)
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		yadrecords = mysqldb.query("SELECT tgt_absPath FROM convert_yadvideos WHERE src_absPath = '#{srcvideo.escapeChar}' AND conversion_status = 1")
		mysqldb.close
		
		if yadrecords.num_rows == 0
			@hbblog.debug "Database management: Unable to find videos that are converted"
			return nil
		elsif yadrecords.num_rows == 1
			@hbblog.debug "Database management: Found one converted video record. "
			return yadrecords.fetch_row.first
		elsif yadrecords.num_rows > 1
			@hbblog.warn "Database management: Found #{yadrecords.num_rows} converted videos in record. Dup? "
			return yadrecords.fetch_row.first
		end
	end
	
	protected
	
	def remainStable(yadrecord)
		# yadrecord = [src_mtime, src_size, src_md5sum, src_absPath] 
		src_mtime, src_size, src_md5sum, src_absPath = yadrecord
		@hbblog.debug "Database management: Verify if #{src_absPath} is remaining stable. "
		begin
			current_mtime = File.mtime(src_absPath).strftime("%Y-%m-%d %H:%M:%S").to_s
			current_size = File.size(src_absPath).to_i 
			current_hash = tailingHash(src_absPath).to_s
			@hbblog.debug "Database management: #{current_mtime} #{current_size} #{current_hash} "
			if 
				(not current_mtime == src_mtime) ||
				(not current_size == src_size.to_i) ||
				(not current_hash == src_md5sum.to_s)
				@hbblog.debug "Database management: #{src_absPath} is unstable. "
				return false
			else
				@hbblog.debug "Database management: #{src_absPath} is remaining stable. "
				return true
			end
		rescue => e
			@hbblog.debug "Database management: Cannot retrieve current file status - #{src_absPath}"
			@hbblog.debug "Database management: #{e}"
		end
	end
	
	def increaseStableStatus(videoPath)
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		yadvideos = mysqldb.query("SELECT src_pass1, src_pass2, src_pass3, src_stable FROM src_yadvideos WHERE src_absPath = '#{videoPath.escapeChar}'")
		if yadvideos.num_rows == 1
			src_pass1, src_pass2, src_pass3, src_stable = yadvideos.fetch_row
			video_status = src_pass1.to_i + src_pass2.to_i + src_pass3.to_i + src_stable.to_i
			@hbblog.debug "Database management: Increase stable status - #{videoPath} to #{video_status.to_i + 1}"
			case video_status
			when 0
				mysqldb.query("UPDATE src_yadvideos SET 
					src_pass1 = 1, src_pass2 = 0, src_pass3 = 0, src_stable = 0
					WHERE src_absPath = '#{videoPath.escapeChar}'
				")
			when 1
				mysqldb.query("UPDATE src_yadvideos SET 
					src_pass1 = 1, src_pass2 = 1, src_pass3 = 0, src_stable = 0
					WHERE src_absPath = '#{videoPath.escapeChar}'
				")
			when 2
				mysqldb.query("UPDATE src_yadvideos SET 
					src_pass1 = 1, src_pass2 = 1, src_pass3 = 1, src_stable = 0
					WHERE src_absPath = '#{videoPath.escapeChar}'
				")
			when 3
				mysqldb.query("UPDATE src_yadvideos SET 
					src_pass1 = 1, src_pass2 = 1, src_pass3 = 1, src_stable = 1
					WHERE src_absPath = '#{videoPath.escapeChar}'
				")
			else
			end
		else
			@hbblog.error "Database management: Unable to locate source video #{videoPath} in mysql database to increase stablity count"
		end
		mysqldb.close
	end
	
	def backtoPass1(videoPath)
		# Going back to Pass1. Update all mtime / size / chksum info. 
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		current_hash = tailingHash(videoPath)
		current_size = File.size(videoPath)
		current_mtime = File.mtime(videoPath).strftime("%Y-%m-%d %H:%M:%S").to_s
		@hbblog.debug "Database management: Resetting video status for #{videoPath} due to instability. "
		yadrecords = mysqldb.query("UPDATE src_yadvideos SET
			src_pass1 = 0,
			src_pass2 = 0,
			src_pass3 = 0,
			src_stable = 0,
			src_md5sum = '#{current_hash}',
			src_size = '#{current_size}',
			src_mtime = '#{current_mtime}'
			WHERE src_absPath = '#{videoPath.escapeChar}'
		")
		mysqldb.close
	end
	
	def tailingHash(file)
		#
		# Only scanning 210MB max for any files. 
		# This is to avoid running out of memory on large video files. 
		#
		require 'digest/md5'
		f = File.new(file, "r")
		byteSize = File.size(file).to_f
		gb = 1024*1024*1024
		mb = 1024*1024
		if byteSize > 4 * gb
			tailbyteSize = 210 * mb
		else
			tailbyteSize = (byteSize * 0.05).to_i
		end
		f.seek(-tailbyteSize, IO::SEEK_END)
		return Digest::MD5.hexdigest(f.read)
	end
	
	
	def ensureDB
		lock_timeout = 60
		hbbTables = %w{src_yadvideos tgt_yadvideos convert_yadvideos}
		mysqldb = Mysql::new(@dbLogin[:dbhost], @dbLogin[:dbuser], @dbLogin[:dbpass], @dbLogin[:dbname])
		hbbTables.each do |hbbTable|
			dbresult = mysqldb.query("SHOW TABLES like '#{hbbTable}'")
			
			if dbresult.num_rows == 0
				@hbblog.info "Database management: Creating new table - #{hbbTable} "
				case hbbTable
				when "src_yadvideos"
					# Creating source YAD table
					# newrecord = before first pass
					# pass1 = first pass - after detecting new record. mtime check.
					# pass2 = second pass - after first pass stablize. md5 check.
					# pass3 = third pass - ready for convert in the next run. 
					mysqldb.query("
						CREATE TABLE #{hbbTable}
						(
							id INT UNSIGNED NOT NULL AUTO_INCREMENT,
							src_filename varchar(255),
							src_mtime DATETIME,
							src_size BIGINT UNSIGNED,
							src_absPath varchar(2048),
							src_md5sum char(32),
							src_pass1 INT(1),
							src_pass2 INT(1),
							src_pass3 INT(1),
							src_stable INT(1),
							PRIMARY KEY (id),
							INDEX (src_absPath),
							INDEX (src_md5sum)
						) ENGINE=InnoDB
					")


				when "tgt_yadvideos"
					# Creating target YAD table
					mysqldb.query("
						CREATE TABLE #{hbbTable}
						(
							id INT UNSIGNED NOT NULL AUTO_INCREMENT,
							tgt_filename varchar(255),
							tgt_absPath varchar(2048),
							src_absPath varchar(2048),
							tgt_locked INT(1),
							tgt_converted INT(1),
							PRIMARY KEY (id),
							INDEX (tgt_absPath),
							INDEX (src_absPath)
						) ENGINE=InnoDB
					")			

				when "convert_yadvideos"
					# Creating conversion change table
					# conversion_status = 0 running
					# conversion_status = 1 done
					# conversion_status = 2 error ( failed )
					mysqldb.query("
						CREATE TABLE #{hbbTable}
						(
							id INT UNSIGNED NOT NULL AUTO_INCREMENT,
							src_filename varchar(255),
							src_absPath varchar(2048),
							tgt_filename varchar(255),
							tgt_absPath varchar(2048),
							conversion_host varchar(100),
							conversion_timestart DATETIME,
							conversion_timeend DATETIME,
							conversion_status INT(1),
							PRIMARY KEY (id),
							INDEX (src_absPath),
							INDEX (tgt_absPath)
						) ENGINE=InnoDB
					")			
				else
				end
			end
		end
		

		@hbblog.info "Database management: Clean up any existing orphan records if any ... "

		failed_videos = mysqldb.query("SELECT tgt_absPath FROM convert_yadvideos WHERE 
			conversion_timestart < DATE_SUB(NOW(), INTERVAL #{lock_timeout} HOUR) AND
			conversion_status = 0
		")

		mysqldb.query("SET autocommit=0")
		mysqldb.query("START TRANSACTION;")
		failed_videos.each do |rowData|
			datapath = rowData[0]
			@hbblog.debug "Database management: Conversion duration exceed 24 hours for #{datapath}"
			mysqldb.query("DELETE FROM tgt_yadvideos WHERE 
				tgt_locked = 1 AND
				tgt_absPath = '#{datapath.escapeChar}'
			")
		end

		mysqldb.query("UPDATE convert_yadvideos SET conversion_status = 2 WHERE 
			conversion_timestart < DATE_SUB(NOW(), INTERVAL #{lock_timeout} HOUR) AND
			conversion_status = 0
		")
		
		mysqldb.query("COMMIT")		
		
		mysqldb.close
	end
	
end

