class HBBLogging
	require 'logger'
	
	def initialize(loglevel=1, output="STDOUT")
		@hbbappRoot=output
		@loglevel = loglevel
	end
	
	def level
		return @loglevel
	end
	
	def setLevel(loglevel)
		@loglevel = loglevel
	end
	
	def setLogFile(dir)
		@hbbappRoot = dir
		stdout = Logger.new(STDOUT)
		if not File.directory? "#{@hbbappRoot}/var"
			require 'fileutils'
			begin
				FileUtils.mkdir_p ("#{@hbbappRoot}/var")
			rescue => e
				stdout.warn "Log management: Unable to create log directory #{@hbbappRoot/var}"
				stdout.warn "Log management: #{e}"
				stdout.warn "Log management: Defaulting log to STDOUT..."
				@hbbappRoot = "STDOUT"
			end
		end
		if not File.exists? "#{@hbbappRoot}/var/handbrakebatchd.log"
			stdout.debug "Log management: Creating log file - #{@hbbappRoot}/var/handbrakebatchd.log"
			File.new("#{@hbbappRoot}/var/handbrakebatchd.log", "w")
		end
		if not File.writable? "#{@hbbappRoot}/var/handbrakebatchd.log"
			stdout.warn "Log management: Unable to write to log file - #{@hbbappRoot}/var/handbrakebatchd.log"
			stdout.warn "Log management: Defaulting log to STDOUT..."
			@hbbappRoot = "STDOUT"
		end			
	end

	def error(msg)
		log = nil
		if @hbbappRoot != "STDOUT"
			log = Logger.new("#{@hbbappRoot}/var/handbrakebatchd.log")
		else
			log = Logger.new(STDOUT)
		end
		log.level = getLogLevel
		log.error msg
		log.close if @hbbappRoot != "STDOUT"
	end

	def info(msg)
		log = nil
		if @hbbappRoot != "STDOUT"
			log = Logger.new("#{@hbbappRoot}/var/handbrakebatchd.log")
		else
			log = Logger.new(STDOUT)
		end
		log.level = getLogLevel
		log.info msg
		log.close if @hbbappRoot != "STDOUT"
	end
	
	def warn(msg)
		log = nil
		if @hbbappRoot != "STDOUT"
			log = Logger.new("#{@hbbappRoot}/var/handbrakebatchd.log")
		else
			log = Logger.new(STDOUT)
		end
		log.level = getLogLevel
		log.warn msg
		log.close if @hbbappRoot != "STDOUT"
	end
	
	def debug(msg)
		log = nil
		if @hbbappRoot != "STDOUT"
			log = Logger.new("#{@hbbappRoot}/var/handbrakebatchd.log")
		else
			log = Logger.new(STDOUT)
		end
		log.level = getLogLevel
		log.debug msg
		log.close if @hbbappRoot != "STDOUT"
	end

	def getLogLevel
		case @loglevel
		when 0
			return Logger::ERROR
		when 1
			return Logger::WARN
		when 2
			return Logger::INFO
		when 3
			return Logger::DEBUG
		else
			return Logger::INFO
		end
	end
end