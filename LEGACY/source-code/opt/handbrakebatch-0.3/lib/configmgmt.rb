class HBBConfig
	def initialize(dir)
		require "#{dir}/lib/logmgmt"
		@config = {}
		
		# Try to read handbrakebatchd.conf by default
		filename = "handbrakebatchd.conf"
		configFile = "#{dir}/etc/#{filename}"

		@hbblog = HBBLogging.new
		@hbbappRoot = dir
		
		@config[:hbbappRoot] = dir
		return readconfig(configFile)
	end
	
	def getConfig
		return @config
	end
	
	def update_by_input(cmdInput)
		require 'optparse'
		option = OptionParser.new do |opts|
			opts.banner = "Usage: handbrakebatchd [mode_of_operation] [options]"
			opts.separator ""
			opts.separator "Mode of operation:"
			opts.separator ""
			opts.on("-d", "--daemon",
				"Daemon mode: Running handbrakebatchd as a server (default)",
				"             It watches for any new videos in 'sourceDir' for",
				"             a given 'scanperiod' (See scanperiod option)" ) do |d|
				@config[:mode] = "daemon"
			end
			opts.on("-i", "--interactive",
				"Interactive mode: Running handbrakebatchd in interactive. ",
				"                  Similar to daemon mode but only ",
				"                  run one-off conversion" ) do |i|
				@config[:mode] = "interactive"
			end
			opts.separator ""
			opts.on("", "--scan", "Scan mode: Similar to interactive but ",
				                  "           does not do any conversion" ) do |scan|
				@config[:mode] = "scan"
			end
			opts.on("", "--noop", "Noop mode: Same as scan mode. " ) do |noop|
				@config[:mode] = "scan"
			end
			opts.on("", "--test", "Test mode: Same as scan mode. " ) do |test|
				@config[:mode] = "scan"
			end
			opts.on("", "--resetDB", "Reset DB: Wipe out all conversion records and scanned data in the database . " ) do |test|
				@config[:mode] = "resetDB"
			end
			opts.separator ""
			opts.separator "Standard options: "
			opts.separator ""
			opts.on("-C", "--config_file FILE", "Specify a config file to use for handbrakebatchd ",
			 							"handbrakebatchd will take all command line options ",
			 							"and then take any options set in specified config file" ) do |file|
				@config[:config_file] = file
			end
			opts.on("-s", "--source DIRECTORY", "Specify source directory for video search and conversion.") do |s|
				@config[:sourceDir] = s.gsub(/\/$/, '')
			end
			opts.on("-t", "--target DIRECTORY", "Specify target directory for converted videos to write to.") do |t|
				@config[:targetDir] = t.gsub(/\/$/, '')
			end
			opts.on("", "--mysqlHost HOST", "Specify mysql host to be used for handbrakebatchd backend") do |host|
				@config[:mysqlHost] = host
			end
			opts.on("", "--mysqlUser USERNAME", "Specify mysql user to be used for handbrakebatchd backend") do |user|
				@config[:mysqlHost] = user
			end
			opts.on("", "--mysqlPass PASSWORD", "Specify mysql password to be used for handbrakebatchd backend") do |pass|
				@config[:mysqlPass] = pass
			end
			opts.on("", "--mysqlDB DBNAME",  "Specify mysql Database to be used for handbrakebatchd backend") do |dbname|
				@config[:mysqlDB] = dbname
			end
			opts.on("", "--combineThreshold SECOND", "This specify a threshold (in seconds) for handbrakebatchd to combine videos", 
			                                  "e.g. Set to 180 will cause handbrakebatchd to combine all videos in 'sourceDir' that", 
			                                  "     are less than 180 secs or 3 minutes in duration. ", 
			                                  "     The new combined / merged video will be named 'DIRECTORYNAME-combined' ",
			                                  "Default value is 180" ) do |time|
				@config[:combineThreshold] = time
			end
			opts.on("", "--loglevel NUM", "Set loglevel for handbrakebatchd. Valid vales are 0-3. Default value is '2' ",
										"    0: error, 1: warning, 2: info, 3: debug" ) do |loglevel|
				@config[:loglevel] = loglevel.to_i
			end
			opts.on("", "--debug", "Run in debug mode. This is same as using --loglevel 3") do |debug|
				@config[:loglevel] == 3
			end
			opts.on("-h", "--help", "Show help screen.") do 
				puts opts
				exit 0
			end
			#opts.separator ""
			#opts.separator "Conversion options: "
			#opts.separator ""
			opts.separator ""
			opts.separator "Daemon options: "
			opts.separator ""
			opts.on("", "--scanperiod SECOND", "Scan period: This is a scan period that the daemon will wake up and scan for new videos",
			        "This option only applies to daemon mode" ) do |sec|
			        @config[:scanfreq] = sec.to_i
			opts.separator ""
			opts.separator ""
			end
		end
		
		begin
			option.parse!(cmdInput)
			require "#{@hbbappRoot}/lib/logmgmt"
			@hbblog.setLevel(@config[:loglevel])
			@hbblog.setLogFile(@hbbappRoot) if @config[:mode] == 'daemon'
		rescue OptionParser::ParseError => e
			@hbblog.error ""
			@hbblog.error "Error parsing command line option"
			@hbblog.error "#{e.to_s}"
			@hbblog.error ""
			@hbblog.error option
		end
			
		@hbblog.info("Starting HandBrakeBatchd ...")
		@hbblog.debug("The following options are passed to HandBrakeBatchd: ")
		@config.each do |attr, value|
			@hbblog.debug "#{attr}: #{value}"
		end
		return @config if isGoodConfig?
		return {} if not isGoodConfig?
	end
	
	protected
	
	def isGoodConfig?
		# future to-do list
		return true
	end
	
	def readconfig(file)
		if File.readable? file
		
			# Reading config file
			require 'parseconfig'
			parse = ParseConfig.new(file)
			@config[:sourceDir] = parse.get_value('sourceDir').gsub(/\/$/, '')
			@config[:targetDir] = parse.get_value('targetDir').gsub(/\/$/, '')
			@config[:presetOption] = parse.get_value('presetOption')
			@config[:supportedType] = parse.get_value('supportedType')
			@config[:mysqlHost] = parse.get_value('mysqlHost')
			@config[:mysqlUser] = parse.get_value('mysqlUser')
			@config[:mysqlPass] = parse.get_value('mysqlPass')
            @config[:convertbadmtime] = parse.get_value('convertbadmtime')
			@config[:mysqlDB] = parse.get_value('mysqlDB')]
			@config[:HBoption] = parse.get_value('hboption')
			@config[:HBoption] = parse.get_value('HBoption') if @config[:HBoption].nil? or @config[:HBoption].empty?
			@config[:HBcommand] = parse.get_value('hbcommand')
			@config[:HBcommand] = parse.get_value('HBcommand') if @config[:HBcommand].nil? or @config[:HBcommand].empty?
			@config[:scanfreq] = parse.get_value('scanperiod').to_i # in sec
			@config[:combineThreshold] = parse.get_value('combineThreshold') # in sec
			@config[:loglevel] = parse.get_value('loglevel').to_i
			
			# Set default values and behaviour if not set
			@config[:presetOption] = 'AppleTV' if @config[:presetOption] == nil
            @config[:convertbadmtime] = false if @config[:convertbadmtime] == nil
			@config[:loglevel] = 1 if @config[:loglevel] == nil
			@config[:scanfreq] = 1800 if (@config[:scanfreq] == nil) or (@config[:scanfreq] == 0)
			@config[:combineThreshold] = 180 if @config[:combineThreshold] == nil
			@config[:HBoption] = '' if @config[:HBoption].nil? or @config[:HBoption].empty?
			@config[:HBcommand] = "HandBrakeCLI" if @config[:HBcommand].nil? or @config[:HBcommand].empty?
			@config[:mode] = "daemon"

			return @config if isGoodConfig?
			return {} if not isGoodConfig?
		else
			@hbblog.warn("Unable to read config file: #{file}")
			return {}
		end
	end
end
