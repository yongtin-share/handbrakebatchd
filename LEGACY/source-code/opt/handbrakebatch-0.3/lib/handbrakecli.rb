class HBBcli
	def initialize(hbbconfigHash)
		@config = hbbconfigHash
		
		require "#{@config[:hbbappRoot]}/lib/logmgmt"
		@hbblog = HBBLogging.new
		@hbblog.setLevel(@config[:loglevel])
		@hbblog.setLogFile(@config[:hbbappRoot]) if @config[:mode] == 'daemon'
		@hbblog.info "HandbrakeCLI management: Starting HandbrakeCLI management ... "
		
	end

	def mcbitrate_detection(srcpath,fps)
		#
		# Only used for rmvb videos
		#
		require 'open3'
		stdin,stdout,stderr = Open3.popen3("ffmpeg -i '#{srcpath}'")
		stdin.puts("exit")
		while ffmpeg = stderr.gets
			# Search for this in the output -> Stream #0.1: Video:
			if ffmpeg =~ /Stream #.*: Video:/
				bitrate = $1 if ffmpeg =~ /, (\d+) kb\/s,/
				tbr = $1 if ffmpeg =~ /kb\/s, (\d+) tbr,/
				tbn = $1 if ffmpeg =~ /tbr, ([\d\w]+) tbn,/
				tbc = $1 if ffmpeg =~ /tbn, ([\d]+) tbc/

				# Assuming all values are retrieved
				begin
					bitperframe = bitrate.to_f / tbr.to_f
					newbitrate = (bitperframe * fps).round(0)
					return newbitrate
				rescue => e
					@hbblog.warn "HandbrakeCLI management: Failed to detect bitrate. #{e}"
					@hbblog.warn "HandbrakeCLI management: setting to 2048 bitrate"
					return 2048.to_f
				end
			end
		end
	end
	
	def convert(srcpath, tgtpath)
		begin
			@hbblog.info "HandbrakeCLI management: Converting video #{srcpath} to #{tgtpath}"
			commandstatus = nil
			if not isRealMedia?(srcpath)
				@hbblog.debug "HandbrakeCLI management: Using HandbrakeCLI to perform conversion. "
				@hbblog.debug "HandbrakeCLI management: #{@config[:HBcommand]} #{@config[:HBoption]} -i #{srcpath} -o #{tgtpath} --preset #{@config[:presetOption]} > /dev/null"
				out = %x{
			
				 	#{@config[:HBcommand]} #{@config[:HBoption]} -i "#{srcpath}" -o "#{tgtpath}" --preset "#{@config[:presetOption]}" > /dev/null
			
				}
				commandstatus = $?.exitstatus
			else
				@hbblog.debug "HandbrakeCLI management: Using Mencoder/HandbrakeCLI to perform conversion. "
				fps = 29.97
				bitrate = mcbitrate_detection(srcpath, fps)
				srcmencoder = tgtpath.gsub(/.mp4$/i, ".mencoder.mp4")
				
				mencoder_option = "-msglevel all=1"
				mencoder_audio = "-oac mp3lame -lameopts preset=128"
				mencoder_video = "-ovc lavc -lavcopts vcodec=mpeg4:vbitrate=#{bitrate}:v4mv:mbd=2:dia=2:ildct:trell:nr=100:qns=2"
				mencoder_fps = "-ofps #{fps}" 

				mencoder_cmd = "mencoder #{mencoder_option} '#{srcpath}' #{mencoder_audio} #{mencoder_video} #{mencoder_fps} -o '#{srcmencoder}'"
				remove_mencoder_file = "/bin/rm -f '#{srcmencoder}'"

				@hbblog.debug "HandbrakeCLI management: #{mencoder_cmd}"
				@hbblog.debug "HandbrakeCLI management: #{@config[:HBcommand]} #{@config[:HBoption]} -b #{bitrate} -i '#{srcmencoder}' -o '#{tgtpath}' --preset '#{@config[:presetOption]}'"
				@hbblog.debug "HandbrakeCLI management: #{remove_mencoder_file}"
				@hbblog.info "HandbrakeCLI management: Executing ..."
				out = %x{

					#{mencoder_cmd}  &&
					#{@config[:HBcommand]} #{@config[:HBoption]} -b #{bitrate} -i "#{srcmencoder}" -o "#{tgtpath}" --preset "#{@config[:presetOption]}" > /dev/null  &&
					#{remove_mencoder_file}

				}
				commandstatus = $?.exitstatus

			end
	
			if commandstatus != 0
				@hbblog.debug "HandbrakeCLI management: convert command status non-zero: #{commandstatus}"
				raise "HandBrakeCLI exited with #{$?} status"
				raise "HandBrakeCLI: #{out}"
				return FALSE
			else
				@hbblog.debug "HandbrakeCLI management: convert command status returning zero: #{commandstatus}"
				return TRUE
			end
		rescue => e
			@hbblog.warn "HandbrakeCLI management: Converting video #{srcpath} to #{tgtpath} failed"
			@hbblog.warn "HandbrakeCLI management: #{e}"
			return FALSE
		end
	end
	
	protected
	
	def isRealMedia?(path)
		# This is not being used anymore.
		if path =~ /(.rm$|.rmvb$)/i
			return true
		else
			return false
		end
	end
end

