class HBBConvertMgmt
	def initialize(hbbconfigHash)
		@config = hbbconfigHash
		
		require "#{@config[:hbbappRoot]}/lib/logmgmt"
		require "#{@config[:hbbappRoot]}/lib/configmgmt"

		@hbblog = HBBLogging.new
		@hbblog.setLevel(@config[:loglevel])
		@hbblog.setLogFile(@config[:hbbappRoot]) if @config[:mode] == 'daemon'

		@hbblog.info "Conversion management: Starting Conversion management system ..."
		
		require "#{@config[:hbbappRoot]}/lib/dbmgmt"
		@hbbDB = HBBdbmgmt.new(@config)
	
		require "#{@config[:hbbappRoot]}/lib/handbrakecli"
		@hbbRun = HBBcli.new(@config)

	end

	def runConvert(videoAry)
		if ensureTargetRootDirExist
			videoAry.each do |sourceVideoPath|
				newVideoPath = makeNewVideoPath(sourceVideoPath)
				if ensurePath(newVideoPath)
					begin
						# Future version 0.3.1: Enable core / thread detection.
						#     current challenge fork impact robustthread operation.
						#hbbconvert_pid = fork { 
							if not @hbbDB.isLocked?(newVideoPath)
								convertFlag = false
								if (not @hbbDB.hasConverted?(sourceVideoPath, newVideoPath)) 
									@hbblog.debug "Conversion management: Proceed with conversion - #{newVideoPath}"
									convertFlag = true
								elsif @hbbDB.hasConverted?(sourceVideoPath, newVideoPath) && (not goodIntegrity(sourceVideoPath))
									convertFlag = true
									@hbblog.debug "Conversion management: Remove previous records and re-converting - #{newVideoPath}"
									@hbbDB.removeConvert(sourceVideoPath, newVideoPath)
								elsif @hbbDB.hasConverted?(sourceVideoPath, newVideoPath) && goodIntegrity(sourceVideoPath)
									@hbblog.debug "Conversion management: Skipping good converted videos -  #{newVideoPath}"
									convertFlag = false
								end

								if convertFlag
									@hbbDB.lockConvert(sourceVideoPath, newVideoPath) 								
									convert_success = @hbbRun.convert(sourceVideoPath, newVideoPath)
									if convert_success
										@hbbDB.unlockConvert(sourceVideoPath, newVideoPath)
										updateVideomtime(sourceVideoPath, newVideoPath)		
									else
										@hbbDB.removeConvert(sourceVideoPath, newVideoPath)
									end
								end
								
							else
								# Probably is being converted somewhere else
								@hbblog.debug "Conversion management: Skipping locked conversion - #{newVideoPath}"
							end

						#}
						#Process.waitall
					rescue => e
						@hbblog.error "Conversion management: Encounter error during conversion. "
						@hbblog.error "Conversion management: #{e}"
						@hbbDB.removeConvert(sourceVideoPath, newVideoPath)
					end
				else
					@hbblog.warn "Conversion management: Unable to obtain access to new video path - #{newVideoPath}"
				end
			end
			return true
		else
			@hbblog.error "Conversion management: Unable to access target directory for writing - #{@config[:targetDir]}"
			return false
		end
	end

	def runUpdateStatus(videoAry)		
		sourceVideosStableToConvertAry = Array.new
		videoAry.each do |video|
			newVideoPath = makeNewVideoPath(video)
			@hbblog.debug "Conversion management: Generating new video path - #{newVideoPath}"
			videoISstable = @hbbDB.isStable?(video)
			if videoISstable && (not @hbbDB.hasConverted?(video, newVideoPath)) && (not @hbbDB.isLocked?(newVideoPath))
				@hbblog.info "Conversion management: Found stable video to be converted - #{video}"
				sourceVideosStableToConvertAry.push(video)
			elsif (not videoISstable) && (not @hbbDB.hasConverted?(video, newVideoPath))
				@hbblog.info "Conversion management: Found unstable video - #{video}"
				checkStablity(video)
				@hbblog.info "Conversion management: Updated unstable video - #{video}"
			elsif (not videoISstable) && @hbbDB.hasConverted?(video, newVideoPath)
				@hbblog.warn "Conversion management: Found converted videos with unstable status - #{video}"
			elsif videoISstable && @hbbDB.hasConverted?(video, newVideoPath) && (not goodIntegrity(video))
				@hbblog.warn "Conversion management: Found converted videos with bad mtime. Re-converting - #{video}"
				sourceVideosStableToConvertAry.push(video)
			end
		end
		return sourceVideosStableToConvertAry
	end
	
	protected
	
	def checkStablity(video)
		fileStatus = { :path => video, :mtime => File.mtime(video), :fileSize => File.size(video) }
		@hbbDB.updateStatus(fileStatus) 
	end
	
	def goodIntegrity(video)
		# Look this up via conversion records
		newvideo = @hbbDB.getConvertedVideo(video)
		if newvideo.nil? || (File.mtime(video) != File.mtime(newvideo))
			return false if @config[:convertbadmtime]
			return true if not @config[:convertbadmtime] 
		else
			return true
		end
	end

	def makeNewVideoPath(videoPath)
		origVideoFilename = File.basename(videoPath)
		newVideoFilename = origVideoFilename.gsub(File.extname(origVideoFilename), ".mp4")
		commonRelativePath = videoPath.gsub("#{@config[:sourceDir]}/", "")
		commonRelativeDir = File.dirname(commonRelativePath)
		
		newVideoPath = File.expand_path("#{@config[:targetDir]}/#{commonRelativeDir}/#{newVideoFilename}")
		return newVideoPath
	end
	
	def ensurePath(newvideopath)
		if File.exist? newvideopath
			if File.writable? newvideopath
				return true 
			else
				@hbblog.warn "Conversion management: Unable to write to existing videopath - #{newvideopath}"
				return false
			end
		else
			newdir = File.dirname(newvideopath)
			require 'fileutils'
			begin
				@hbblog.debug "Conversion management: Creating target video path directory - #{newdir}"
				FileUtils.mkdir_p(newdir)
				return true
			rescue => e
				@hbblog.warn "Conversion management: Unable to create target video path directory - #{newdir}"
				@hbblog.warn "Conversion management: #{e}"
				return false
			end
		end
	end
	
	def ensureTargetRootDirExist
		dir = @config[:targetDir]
		if File.directory? dir
			if File.writable? dir
				return true
			else
				@hbblog.error "Conversion management: Target is not a writable directory - #{@config[:targetDir]}"
				return false
			end
		else
			@hbblog.error "Conversion management: Target is not a directory - #{@config[:targetDir]}"
			return false
		end
	end
	
	def updateVideomtime(src,tgt)
		require 'fileutils'
		FileUtils.touch tgt, :mtime => File.mtime(src)
	end
end
