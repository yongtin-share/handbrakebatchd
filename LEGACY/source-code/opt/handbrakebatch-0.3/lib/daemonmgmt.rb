class HBBDaemonMgmt
	def initialize(hbbconfigHash)
		@config = hbbconfigHash
		
		require "#{@config[:hbbappRoot]}/lib/logmgmt"
		require "#{@config[:hbbappRoot]}/lib/configmgmt"

		@hbblog = HBBLogging.new
		@hbblog.setLevel(@config[:loglevel])
		@hbblog.setLogFile(@config[:hbbappRoot]) if @config[:mode] == 'daemon'
		@hbblog.info "Daemon management: Starting Daemon management system ... "
		


	end
	
	def interactive
		@hbblog.info "Daemon management: Begin interactive mode"

		sourceVideosRAWAry = @hbbindex.findSourceFiles
		sourceVideosStableToConvertAry = @hbbconvert.runUpdateStatus(sourceVideosRAWAry)		

		@hbblog.info "Daemon management (interactive):   Starting interactive conversion process ... "
		@hbbconvert.runConvert(sourceVideosStableToConvertAry)
	end
	
	def daemon
		require 'robustthread'
		require 'logger'
		@hbblog.info "Daemon management: Begin daemon mode"
		RobustThread.logger = Logger.new("#{@config[:hbbappRoot]}/var/handbrakebatchd.log")
		RobustThread.logger.level = @hbblog.getLogLevel
		RobustThread.exception_handler do |exception|
			@hbblog.error "Daemon management: Caught an error when running HandBrakeBatchd. Exiting..."
			@hbblog.error "Daemon management: #{exception}"
		end
		RobustThread.add_callback(:before_exit){ 
			@hbblog.info("Daemon management: Stopping HandBrakeBatchd server.") 
		}
		
		@hbblog.info("Daemon management: Starting HandBrakeBatchd server. ")
		hbbpid = fork { 
			RobustThread.loop(:seconds => @config[:scanfreq], :label => "HandBrakeBatchd daemon mode") do

				begin			
					require "#{@config[:hbbappRoot]}/lib/indexing"
					@hbbindex = HBBIndexing.new(@config)

					require "#{@config[:hbbappRoot]}/lib/convertmgmt"
					@hbbconvert = HBBConvertMgmt.new(@config)
				
					@hbblog.debug "Daemon management: Indexing and finding all video data "
					sourceVideosRAWAry = @hbbindex.findSourceFiles
					
					@hbblog.debug "Daemon management: Run status update with to get stable video listing"
					sourceVideosStableToConvertAry = @hbbconvert.runUpdateStatus(sourceVideosRAWAry)	
					
					convertOK = @hbbconvert.runConvert(sourceVideosStableToConvertAry)
					@hbblog.info("Daemon management: Scan completed at #{Time.now.to_s}. ")
					@hbblog.debug "Daemon management: Next scan run in #{@config[:scanfreq]} seconds at #{(Time.now + @config[:scanfreq]).to_s}"
					if not convertOK
						raise "Daemon management: Failed running handbrakebatchd. "
					end
				rescue => e
					@hbblog.error "Daemon management: Encounter error when scanning ..."
					@hbblog.error "Daemon management: #{e}"
				end
					
			end
			sleep
		}
		Process.detach hbbpid
	end
	
	def scan
		@hbblog.info "Daemon management: Begin scan mode"
		sourceVideosRAWAry = @hbbindex.findSourceFiles
		sourceVideosStableToConvertAry = @hbbconvert.runUpdateStatus(sourceVideosRAWAry)
		showVideos(sourceVideosStableToConvertAry)
	end
	
	def resetDB
		@hbblog.info "Daemon management: Reset database records"
		
		require "#{@config[:hbbappRoot]}/lib/dbmgmt"
		@hbbDB = HBBdbmgmt.new(@config)
		
		@hbbDB.resetDB
	end
	
	protected
	
	def showVideos(array)
		@hbblog.info "Daemon management: displaying videos status"
		array.each do |video|
			@hbblog.info "Daemon management: #{video.name} will be converted. "
		end
	end
	
end