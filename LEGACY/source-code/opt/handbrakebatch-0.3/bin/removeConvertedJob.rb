#! /usr/bin/env ruby
require 'mysql'
require 'parseconfig'

filename = "handbrakebatchd.conf"
hbbRoot = File.dirname(File.dirname(File.expand_path(__FILE__)))
configFile = "#{hbbRoot}/etc/#{filename}"

data = ParseConfig.new(configFile)
sqlServer = data.get_value("mysqlHost")
sqlUser = data.get_value("mysqlUser")
sqlPassword = data.get_value("mysqlPass")
sqlDB = data.get_value("mysqlDB")
mysqldb = Mysql::new(sqlServer, sqlUser, sqlPassword, sqlDB)

mode = "-v"
if ARGV.length == 1
	search_string = ARGV[0]
elsif ARGV.length == 2
	search_string = ARGV[0]
	mode = ARGV[1]
else
	puts "Error on argument. "
	puts "Usage: removeConvertedJob.rb <video> <-r|-v>"
	exit 1
end
remove_videos = mysqldb.query("SELECT id,tgt_filename FROM tgt_yadvideos WHERE tgt_filename LIKE '%#{search_string}%'")
remove_videos.each do |remove_video|
	file = remove_video[1]
	puts "Video found in the database to be removed => #{file}" if mode == "-v"

	puts "Removing video from the database => #{file} " if mode == "-r"
	mysqldb.query("DELETE FROM tgt_yadvideos WHERE id = #{remove_video[0]}") if mode == "-r"
end

