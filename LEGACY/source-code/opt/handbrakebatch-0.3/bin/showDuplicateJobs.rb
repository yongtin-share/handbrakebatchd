#! /usr/bin/env ruby

require 'mysql'
require 'parseconfig'

filename = "handbrakebatchd.conf"
hbbRoot = File.dirname(File.dirname(File.expand_path(__FILE__)))
configFile = "#{hbbRoot}/etc/#{filename}"

data = ParseConfig.new(configFile)
sqlServer = data.get_value("mysqlHost")
sqlUser = data.get_value("mysqlUser")
sqlPassword = data.get_value("mysqlPass")
sqlDB = data.get_value("mysqlDB")
mysqldb = Mysql::new(sqlServer, sqlUser, sqlPassword, sqlDB)

# Find all duplicate records from convert_yadvideos
duplicateJobs = mysqldb.query("SELECT count(*),tgt_absPath,tgt_filename,conversion_host FROM convert_yadvideos GROUP BY
	tgt_absPath having count(*) > 1;
")

duplicateJobs.each do |dupJob|
	count, absPath, filename, conversionHost = dupJob
	puts "convert_yadvidoes: #{filename}: have convered #{count} times"
	oldConvertRecords = mysqldb.query("SELECT tgt_filename,conversion_timestart,conversion_timeend,conversion_status,conversion_host FROM convert_yadvideos WHERE tgt_absPath = '#{absPath}' AND conversion_status != 2")
	oldConvertRecords.each do |record|
		filename, timestart, timeend, status, host = record
		printf("%35s:%2s ===> %15s - %15s\n", host,status,timestart,timeend)
	end
	printf("\n")
end

print "\n\n"
# Find duplicate records from tgt_yadvideos
duplicateJobs = mysqldb.query("SELECT count(*),tgt_absPath,tgt_filename FROM tgt_yadvideos GROUP BY tgt_absPath having count(*) > 1;")
duplicateJobs.each do |dupJob|
	count, absPath, filename = dupJob
	puts "tgt_yadvideos: #{absPath}: have #{count} records"
	targetRecords = mysqldb.query("SELECT tgt_locked, tgt_converted FROM tgt_yadvideos where tgt_absPath = '#{absPath}'")
	targetRecords.each do |target|
		locked, converted = target 
		puts "\t Lock Status: #{locked} Converted Status: #{converted}"
	end
end
