#
# Video class for handling, analyzing, and converting a video with logger function
# 
class Video 
	require 'sqlite3'
	require 'logger'
	
	# Method new for Video
	def initialize(source)
		@sourcePath = source
		@srcfilename = File.basename(source)
		@targetfilename = @srcfilename.gsub(File.extname(@srcfilename), ".mp4")
		
		relativePath = source.gsub(@@sourceRoot, "")
		targetDirectory = File.dirname("#{@@targetRoot}#{relativePath}")
		if not File.exist? targetDirectory
			require 'fileutils'
			FileUtils.mkdir_p "#{targetDirectory}"
		end
		@targetPath = "#{targetDirectory}/#{@targetfilename}"
		
		@videoLog = Logger.new(@@logfile, 5, 1024000)
		@videoLog.datetime_format = "%Y-%m-%d %H:%M:%S "
		@videoLog.level = Logger::WARN if not @@verbose
		@videoLog.level = Logger::INFO if @@verbose
	end

	# Class method to show status bar if is interactive mode (should be in HandBrakeBatch class?)
	def self.updateStatusbar(process, total)
		cr = "\r"
		clear = "\e[0K"
		reset = cr + clear
		percent = process.to_f / total.to_f * 100.0
		print "#{reset}Scanning and Converting in progress: #{percent.to_i}%" 
		print "#{reset}Scanned completed on #{@@sourceRoot}. \n" if process == total
	end

	# Class method to set up preset_options and supported filetype for the entire class
	def self.setConfig(config_hash)
		@@sourceRoot = config_hash[:sourceDir]
		@@targetRoot = config_hash[:targetRoot]
		@@presetOption = config_hash[:presetOption]
		@@supportedType = config_hash[:supportedType]
		@@dbfile = config_hash[:dbfile]
		@@dbTable = config_hash[:dbTable]
		@@hbcommand = config_hash[:hbcommand]
		@@logfile = config_hash[:logfile]
		@@verbose = config_hash[:verbose]
	end
	
	# Class method determine whether the input is a supported filetype
	def self.isSupportedFormat?(file)
		extFilename = File.extname(file)
		if @@supportedType =~ /#{extFilename}/i 
			return TRUE
		else
			return FALSE
		end
	end
	
	# Instance method: Determine if video object has been converted (based on sourcefile and DB)
	def hasConverted?
		db = SQLite3::Database.new ( @@dbfile )
		begin
			videostatus = db.execute("SELECT videostatus FROM #{@@dbTable} WHERE absPath = '#{@sourcePath}'")[0][0].to_i
			db.close
		rescue
			db.close
			videostatus = -1
			@videoLog.info "Cannot retrieve Video infomation from database. "
			@videoLog.info "\tVideo #{@srcfilename} might not exist in the DB previously."
		end
		return TRUE if videostatus == 4
		return FALSE if videostatus != 4
	end
	
	# Instance method: Determine if source video is stable (size / mtime not changing) for the last three checks
	def isStable?
		db = SQLite3::Database.new ( @@dbfile )
		begin
			videostatus = db.execute("SELECT videostatus FROM #{@@dbTable} WHERE absPath = '#{@sourcePath}' ")[0][0].to_i
			db.close
		rescue
			db.close
			videostatus = -1
			@videoLog.info "Cannot retrieve Video infomation from database. "
			@videoLog.info "\tVideo #{@srcfilename} might not exist in the DB previously."
		end
		return TRUE if videostatus >= 3
		return FALSE if videostatus < 3
	end
	
	# Instance method for converting the video object
	def convert
		begin
			@videoLog.info ""
			@videoLog.info "Converting video #{@sourcePath} ..."
			@videoLog.info ""
			`#{@@hbcommand} -i "#{@sourcePath}" -o "#{@targetPath}" --preset "#{@@presetOption}"`
			updateRecord
		rescue
			# Log the error and continue
			@videoLog.warn ""
			@videoLog.warn "HandbrakeCLI error while processing Video: #{@sourcePath}"
			@videoLog.warn "\tcommand: #{@@hbcommand} -i #{@sourcePath} -o #{@targetPath} --preset #{@@presetOption}"
			@videoLog.warn ""
		end
	end

	# Instance method for display Video Infomation (scan mode)
	def displayInfo
		db = SQLite3::Database.new ( @@dbfile )
		inDB = false
		begin
			videorecord = db.execute("SELECT id FROM #{@@dbTable} WHERE absPath = '#{@sourcePath}' ")
			inDB = true if videorecord.length == 1 # existing record
		rescue
			inDB = false
		end
		db.close
		print "New video found: \"#{@sourcePath}\"\n" if not inDB
	end
	
	# Instance method for prompt for Video conversion
	def promptAction
		return true
	end
	
	# Instance method for update DB record for Video stablization
	def wait2stable
		db = SQLite3::Database.new ( @@dbfile )
		
		# Check to see if record exists. 
		create_db if not db_exists?
		videorecord = db.execute("SELECT id FROM #{@@dbTable} WHERE absPath = '#{@sourcePath}' ")
		
		if videorecord.length == 1 # This is existing record. Use UPDATE
			stable = FALSE
			
			# Verify if file has changed (mtime / size)
			thisVideoID, thisVideoMtime, thisVideoSize = db.execute("SELECT id,mtime,size FROM #{@@dbTable} WHERE absPath = '#{@sourcePath}' ")[0]
			stable = TRUE if thisVideoMtime == "#{File.mtime(@sourcePath)}" and thisVideoSize == "#{File.size(@sourcePath)}"
			
			# If Video is stable
			if stable
				db.execute("UPDATE #{@@dbTable} SET videostatus = videostatus + 1 where id = #{thisVideoID}") 
				@videoLog.info "Video file #{@sourcePath} pass this stability scan. Pending for the next stability scan. " 
			end
			
			# If Video is not stable
			if not stable
				db.execute("UPDATE #{@@dbTable} SET 
					videostatus = 1, 
					mtime = '#{File.mtime(@sourcePath)}', 
					size = '#{File.size(@sourcePath)}'
					where id = #{thisVideoID}") 
				@videoLog.warn "Video file #{@sourcePath} is changing its status. Reset stability scan counts. " 
			end
			
		elsif videorecord.length == 0 # This is new record. Use INSERT  
			#require 'digest/md5'
			
			@videoLog.info "Found new video file #{@sourcePath}. Pending for the next stability scan. "
			db.execute("INSERT INTO #{@@dbTable} (srcfilename, targetfilename, mtime, size, absPath, md5sum) VALUES
				( '#{@srcfilename}',
				  '#{@targetfilename}',
				  '#{File.mtime(@sourcePath)}',
				  '#{File.size(@sourcePath)}',
				  '#{@sourcePath}',
				  '0'
				)
			")
			#'#{Digest::MD5.hexdigest(File.read(@sourcePath))}'
		end
		db.close
	end

	# methods below are being used only within class internally
	protected
	
	def db_exists?
		db = SQLite3::Database.new ( @@dbfile )
		tableExist = db.execute("SELECT * FROM sqlite_master WHERE type = 'table' AND name LIKE '#{@@dbTable}'")
		db.close
		return FALSE if tableExist.length == 0
		return TRUE if tableExist.length != 0
	end
	
	def create_db
		# Create table if not exists
		@videoLog.warn "Database has not been initialized yet. Creating database #{@@dbTable} .... "
		
		# videostatus = 0 (new record)
		# videostatus = 1 (first pass)
		# videostatus = 2 (second pass)
		# videostatus = 3 (third pass - Ready to convert)
		# videostatus = 4 (Converted)
		db = SQLite3::Database.new ( @@dbfile )
		db.execute("CREATE TABLE #{@@dbTable} 
			( id INTEGER NOT NULL PRIMARY KEY,
			  srcfilename varchar(255),
			  targetfilename varchar(255),
			  mtime varchar(255),
			  size INTEGER,
			  absPath varchar(1024),
			  md5sum varchar(255),
			  videostatus INTEGER DEFAULT 0
			) 
		") 
		@videoLog.warn "Database has been initialized. "
		@videoLog.warn "\tCreated database #{@@dbTable} in database file #{@@dbfile}. "
		db.close
	end
	
	# Instance method for updateRecord once Video has been converted
	def updateRecord
		@videoLog.info "Updating video conversion record into the database #{@@dbTable}. "
		db = SQLite3::Database.new ( @@dbfile )
		
		# Check to see if record exists. 
		create_db if not db_exists?
		videorecord = db.execute("SELECT id FROM #{@@dbTable} WHERE absPath = '#{@sourcePath}' ")
		
		# This is existing record. Use UPDATE
		if videorecord.length == 1 
			thisVideoID = videorecord[0][0]
			db.execute("UPDATE #{@@dbTable} SET videostatus = 4 where id = #{thisVideoID}") 
		
		# This is new record. Use INSERT  
		elsif videorecord.length == 0 	
			db.execute("INSERT INTO #{@@dbTable} (srcfilename, targetfilename, mtime, size, absPath, md5sum) VALUES
				( '#{@srcfilename}',
				  '#{@targetfilename}',
				  '#{File.mtime(@sourcePath)}',
				  '#{File.size(@sourcePath)}',
				  '#{@sourcePath}',
				  '4'
				)
			")
		end
		@videoLog.info "Video conversion record updated for video #{@targetfilename}. "
		db.close
	end
	
end
