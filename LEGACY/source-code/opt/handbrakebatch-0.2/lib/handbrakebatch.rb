#
# To do list:
# 	Fine Tune output (interactive mode)
#
class HandBrakeBatch 

	# Class method to set up preset_options and supported filetype for the entire class
	def self.setConfig(myAppRoot)
		# ------------ Bug: Check config location and existence --------------
		
		# Hash of config options
		@@config_options = {}
		@@config_options[:appRoot] = myAppRoot
		config_file = "#{myAppRoot}/etc/handbrakeBatch.conf"
		@@config_options[:configfile] = config_file
		
		
		# Verify if config_file exists
		if ! File.readable? config_file
			print "[Error]: Cannot locate handbrakeBatch.conf file in the following location: \n" 
			print "\t #{config_file} \n"
			exit 145
		end

		# Using parseconfig to parse config file
		require 'parseconfig'
		config = ParseConfig.new(config_file)
		@@config_options[:sourceDir] = config.get_value('sourceDir')
		@@config_options[:targetRoot] = config.get_value('targetDir')
		@@config_options[:presetOption] = config.get_value('presetOption')
		@@config_options[:supportedType] = config.get_value('supportedType')
		@@config_options[:dbfile] = config.get_value('dbfile')
		@@config_options[:dbTable] = config.get_value('dbTable')
		@@config_options[:hbcommand] = config.get_value('hbcommand')
		@@config_options[:logfile] = config.get_value('logfile')
		@@config_options[:scanfreq] = config.get_value('scanfreq').to_i

		# Set default values for config_options
		@@config_options[:presetOption] = "AppleTV" if @@config_options[:presetOption] == nil
		@@config_options[:dbTable] = "HandBrakeBatch" if @@config_options[:dbTable] == nil
		@@config_options[:dbfile] = "#{@@config_options[:appRoot]}/var/handbrakeBatchd.db" if @@config_options[:dbfile] == nil
		@@config_options[:daemonlogfile] = "#{@@config_options[:appRoot]}/var/handbrakeBatchd.log" if @@config_options[:daemonlogfile] == nil
		@@config_options[:logfile] = "#{@@config_options[:appRoot]}/var/handbrakeBatchd.log" if @@config_options[:logfile] == nil
		@@config_options[:scanfreq] = 1800 if @@config_options[:scanfreq] == nil # in seconds

		
		# Default behaviour is run as server. 
		@@config_options[:mode] = 0
		@@config_options[:verbose] = false
		
	end
	
	# Class method for parsing Options
	def self.parseOptions(args)
		require 'optparse'
		optParse = OptionParser.new do |opts|
			opts.banner = "Usage: handbrakebatchd [options]"

			opts.separator ""
			opts.separator "Mode of operation:"

			opts.on("-d", "--daemon", 
				"Run handbrakebatchd in server mode. This is the default behaviour.", 
				"Daemon mode watch for any new videos in specified directories.") do 
				@@config_options[:mode] = 0
			end
			opts.separator ""
			opts.on("-i", "--interactive", 
				"Run handbrakebatchd in interactive mode.", 
				"Interactive mode run one off conversion of all videos found ",
				"in specified directories.") do 
				@@config_options[:mode] = 1
				#@@config_options[:logfile] = STDOUT
			end
			opts.separator ""
			opts.on("-s", "--scan", "Run one off scan and list any files that needs a conversion.") do
				@@config_options[:mode] = 2
			end

			opts.separator ""
			opts.separator "Standard options:"

			opts.on("-c", "--config FILE", "Specify config file for HandBrakeBatchd.") do |file|
				@@config_options[:configfile] = file
			end

			opts.on("-l", "--log FILE", "Specify log file for HandBrakeBatchd.") do |file|
				@@config_options[:logfile] = file
			end
			
			opts.on("-v", "--verbose", 
				"Run handbrakebatchd in verbose mode.", 
				"This gives more logging infomation either in output or logfile.") do
				@@config_options[:verbose] = true
			end						

			opts.on("-h", "--help", "Show help screen.") do |i|
				puts opts
				exit 
			end
			
			opts.separator ""
			opts.separator "Conversion options:"
			
			opts.on("-s", "--source", "Specify source directory for video search and conversion.") do |s|
				@@config_options[:sourceDir] = s
			end
			
			opts.on("-t", "--target", "Specify target directory for converted videos to write to.") do |t|
				@@config_options[:targetRoot] = t
			end
			
			opts.on("-b", "--preset", 
				"Specify HandBrake preset options.", 
				"Details available via HandBrakeCLI command") do |p|
				@@config_options[:presetOption] = p
			end
			
			opts.separator ""
			opts.separator "Daemon options: "
			
			opts.on("--scanperiod SEC", "The daemon will scan the sourceDir every SEC seconds for any new videos. ") do |sec|
				@@config_options[:scanfreq] = sec
			end

		end
		optParse.parse!(args)
	end
	
	def self.convert()
		# verify inputs before running conversion. 
		verifyInput
		
		# Setup configuration for Video class
		require "#{@@config_options[:appRoot]}/lib/video"
		Video.setConfig(@@config_options)
		
		# Index all files and stored in pathlists for searching
		require 'find'
		pathlists = Find.find(@@config_options[:sourceDir])
		
		# If run as interactive mode (one time run), 
		#    find all supported files that has not been converted, 
		#    prompt for conversion, 
		#    and convert it. 
		if @@config_options[:mode] == 1
			# First, index the sourceDir for total files. 
			numFiles = 0
			processedFiles = 0
			pathlists.each do |path|
				if ! File.directory?path
					numFiles += 1 if Video.isSupportedFormat?path
				end
			end
			Video.updateStatusbar(processedFiles, numFiles)
			
			# Then, actually analysis each files.
			pathlists.each do |path|
				if ! File.directory?path
					if Video.isSupportedFormat?path
						processedFiles += 1
						video = Video.new(path)
						if not video.hasConverted? and video.promptAction == true
							video.convert
						end
						Video.updateStatusbar(processedFiles, numFiles)
					end
					
				end
			end

		
			
		# If run as scan mode (one time scan), 
		#    find all supported files that has not been converted, 
		#    list those (sourcePath) that should be converted.
		elsif @@config_options[:mode] == 2
			pathlists.each do |path|
				if ! File.directory?path
					video = Video.new(path)
					if Video.isSupportedFormat?path and not video.hasConverted? 
						video.displayInfo
					end
				end
			end

			
		# If run as daemon (continuous run), 
		#    start a thread, loop for a time period "scanfreq"
		#       find all supported files that has not been converted, 
		#       verify if sourcePath is stable, 
		#       and convert it. 
		elsif @@config_options[:mode] == 0
			require 'robustthread'
			RobustThread.logger = Logger.new(@@config_options[:daemonlogfile])
			RobustThread.logger.datetime_format = "%Y-%m-%d %H:%M:%S "
			RobustThread.logger.level = Logger::WARN if not @@config_options[:verbose]
			RobustThread.logger.level = Logger::INFO if @@config_options[:verbose]
			RobustThread.exception_handler do |exception|
				puts "\n[Internal error]: Caught an error when running HandBrakeBatchd. Exiting...\n"
				exit 101
			end
			RobustThread.add_callback(:before_exit){ RobustThread.logger.info("Exiting HandBrakeBatchd server ...") }
			
			# Fork a process for daemon
			pid = fork do
				RobustThread.loop(:seconds => @@config_options[:scanfreq], :label => "HandBrakeBatchd") do
					RobustThread.logger.info("Start scanning for new videos ...")
					pathlists.each do |path|
						if ! File.directory?path
							if Video.isSupportedFormat?path
								video = Video.new(path)
								if not video.hasConverted? and video.isStable? 
									video.convert
								elsif not video.isStable?
									video.wait2stable
								end
							end
						end
					end
					RobustThread.logger.info("Scan completed. Next scan in #{@@config_options[:scanfreq]} seconds. ")
				end
				sleep 
			end
			Process.detach pid
		end
		
	end
	
	def self.verifyInput
		# Assume appRoot must exists
		#@@config_options[:appRoot]
		#@@config_options[:dbfile]
		#@@config_options[:logfile] 
		(puts "[Fatal Error]: Config file #{@@config_options[:configfile]} is not readable. "; exit 5) if ! File.readable? @@config_options[:configfile]
		(puts "[Fatal Error]: Directory #{@@config_options[:sourceDir]} is not readable. "; exit 6) if ! File.readable? @@config_options[:sourceDir]
		(puts "[Fatal Error]: Directory #{@@config_options[:targetRoot]} is not writable. "; exit 7) if ! File.writable? @@config_options[:targetRoot] 
		(puts "[Fatal Error]: HandbrakeCLI cannot be found: #{@@config_options[:hbcommand]}"; exit 8) if ! File.executable? @@config_options[:hbcommand] 
		(puts "[Internal Error]: Unknown mode of operation: #{@@config_options[:mode]}"; exit 100) if ! @@config_options[:mode].between?(-1,3)
		puts "Using verbose mode" if @@config_options[:verbose] == true
	end
end
