#! /usr/bin/env ruby

require 'find'

lists = Find.find("./source")
lists.each do |list|
	if ((not File.directory?(list)) and (list =~ /rmvb$/i))
		require 'open3'
		stdin,stdout,stderr = Open3.popen3("ffmpeg -i '#{list}'")
		stdin.puts("exit")
		while ffmpeg = stderr.gets
			# Search for this in the output -> Stream #0.1: Video:
			if ffmpeg =~ /Stream #.*: Video:/
				bitrate = $1 if ffmpeg =~ /, (\d+) kb\/s,/
				tbr = $1 if ffmpeg =~ /kb\/s, (\d+) tbr,/
				tbn = $1 if ffmpeg =~ /tbr, ([\d\w]+) tbn,/
				tbc = $1 if ffmpeg =~ /tbn, ([\d]+) tbc/

				# Assuming all values are retrieved
				begin
					bitperframe = bitrate.to_f / tbr.to_f
					newbitrate = (bitperframe * 29.97).round(0)
					puts "#{list}: #{bitrate} #{tbr} #{tbn} #{tbc} #{newbitrate}" 

					srcmencoder = list.gsub(/.rmvb$/i, ".mencoder.mp4")
					tgtpath = list.gsub(/.rmvb$/i, ".mp4")

					fps = 29.97
					mencoder_audio = "-oac mp3lame -lameopts preset=128"
					mencoder_video = "-ovc lavc -lavcopts vcodec=mpeg4:vbitrate=#{newbitrate}:v4mv:mbd=2:dia=2:ildct:trell:nr=100:qns=2"
					mencoder_fps = "-ofps #{fps}"
					mencoder_cmd = "mencoder -msglevel all=1 '#{list}' #{mencoder_audio} #{mencoder_video} #{mencoder_fps} -o '#{srcmencoder}'"
					remove_cmd = "/bin/rm -f '#{srcmencoder}'"
					out = %x{
						#{mencoder_cmd};
						HandBrakeCLI -O -T -i '#{srcmencoder}' -o '#{tgtpath}' --preset 'AppleTV 2' > /dev/null;
						#{remove_cmd};
					}
				rescue
					puts "#{list}: #{bitrate} #{tbr} #{tbn} #{tbc} 2048" 
				end
			end
		end
	end
end
