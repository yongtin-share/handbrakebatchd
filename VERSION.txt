0.3b
Complete rewrite with mysql backend. Conversion locking. Proper logging. 

0.3a
include ruby / gem dependencies into apps. Packaged up as tarball.

0.2.1
Complete interactive mode function with prompt. 
Fix scan mode to be more verbose (with files that has not been converted).
Add list mode to list converted videos.
More logging when doing video conversion in verbose mode (or regular mode).
Update converted video's timestamp to match source videos. 
Fix a bug where file listings is a static list when run as daemon.
Fix daemon startup script to be more robust.
Ability to remove previously converted videos


0.2
Run daemon mode, interactive mode, and scan mode.
Allow optional arguments in addition to configuration file.
Allow output / logs to go to logfile. 
Bug fix to verify configuration inputs.


0.1
Initial version. 
Run batchRun.rb of Handbrake within a configurable search directory. 
Configuration file in /etc

